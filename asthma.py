'''
                                  ESP Health
                         Notifiable Diseases Framework
                           Asthma  Case Generator


@author: Carolina Chacin <cchacin@commoninf.com>, Bob Zambarano <bzambarano@commoninf.com>
@organization: commonwealth informatics http://www.commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2011 Channing Laboratory
@license: LGPL
'''

# In most instances it is preferable to use relativedelta for date math.  
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from ESP.utils import log
from django.db.models import F, Max, Q
from django.contrib.contenttypes.models import ContentType

from ESP.hef.base import Event
from ESP.hef.base import PrescriptionHeuristic,BaseEventHeuristic
from ESP.hef.base import Dose
from ESP.hef.base import LabResultPositiveHeuristic,LabResultAnyHeuristic

from ESP.hef.base import LabOrderHeuristic
from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case
from ESP.nodis.models import CaseActiveHistory
from ESP.static.models import DrugSynonym



class AsthmaNew(DiseaseDefinition):
    '''
    Asthma new
    '''
    
    conditions = ['asthma']
    
    uri = 'urn:x-esphealth:disease:channing:asthma:v1'
    
    short_name = 'asthma'
    
    test_name_search_strings = [
        
        ]
    
    timespan_heuristics = []
    
    
    @property
    def event_heuristics(self):
        heuristic_list = []
        #
        # Diagnosis Codes
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'asthma',
            dx_code_queries = [
                Dx_CodeQuery(starts_with='J45', type='icd10'),
                Dx_CodeQuery(starts_with='493.', type='icd9'),
                Dx_CodeQuery(starts_with='J46', type='icd10'),
                ]
            ))
        
        #
        # Prescriptions
        #
        heuristic_list.append( PrescriptionHeuristic(
            name = 'albuterol',
            drugs = DrugSynonym.generics_plus_synonyms(['Albuterol','Accuneb','Ventolin','Proventil','Proaire','Vospire','Airomir','Asmavent','Salbutamol']),
            exclude = ['Levalbuterol','Ipratropium', 'Xopenex', 'Budesonide','Combivent','Duoneb'],
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'levalbuterol',
            drugs = DrugSynonym.generics_plus_synonyms(['Levalbuterol','Xopenex']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'pirbuterol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Pirbuterol','Maxair']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'arformoterol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Arformoterol','Brovana']),
            ))    
        heuristic_list.append( PrescriptionHeuristic(
            name = 'formoterol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Formoterol','Foradil','Perforomist','Oxeze']),
            exclude = ['Arformoterol','Mometasone','Budesonide', 'Brovana'],
            ))   
        heuristic_list.append( PrescriptionHeuristic(
            name = 'indacaterol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Indacaterol','Arcapta']),
            ))   
        heuristic_list.append( PrescriptionHeuristic(
            name = 'salmeterol',
            drugs =  DrugSynonym.generics_plus_synonyms(['Salmeterol','Serevent']),
            exclude = ['Fluticasone'],
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'beclomethasone',
            drugs =  DrugSynonym.generics_plus_synonyms(['Beclomethasone','Qvar']),
            ))
        # test that is not going to pick up when it has both aer and inh
        heuristic_list.append( PrescriptionHeuristic(
            name = 'budesonide-inh',
            drugs =  DrugSynonym.generics_plus_synonyms(['Budesonide',]),
            qualifiers = ['INH',' NEB', 'AER'],
            exclude = ['Formoterol','Pulmicort','Albuterol','Dulera','Zenhale'],
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'pulmicort',
            drugs =  DrugSynonym.generics_plus_synonyms(['Pulmicort',]),
            ))       
        heuristic_list.append( PrescriptionHeuristic(
            name = 'ciclesonide-inh',
            drugs =  DrugSynonym.generics_plus_synonyms(['Ciclesonide',]),
            qualifiers = ['INH',' NEB', 'AER'],
            exclude = ['Alvesco'],
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'alvesco',
            drugs =  DrugSynonym.generics_plus_synonyms(['Alvesco',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'flunisolide-inh',
            drugs =  DrugSynonym.generics_plus_synonyms(['Flunisolide',]),
            qualifiers = ['INH',' NEB', 'AER'],
            exclude = ['Aerobid','Aerospan'],
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'aerobid',
            drugs =  DrugSynonym.generics_plus_synonyms(['Aerobid','Aerospan']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fluticasone-inh',
            drugs =  DrugSynonym.generics_plus_synonyms(['Fluticasone','Armonair','Annuity']),
            qualifiers = ['INH',' NEB', 'AER'],
            exclude = ['Salmeterol','Flovent','Vilanterol','Umeclidinium']
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'flovent',
            drugs =  DrugSynonym.generics_plus_synonyms(['Flovent',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'mometasone-inh',
            drugs =  DrugSynonym.generics_plus_synonyms(['Mometasone',]),
            qualifiers = ['INH',' NEB', 'AER'],
            exclude = ['Formoterol','Dulera', 'Zenhale', 'Asmanex'],
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'asmanex',
            drugs =  DrugSynonym.generics_plus_synonyms(['Asmanex',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'montelukast',
            drugs =  DrugSynonym.generics_plus_synonyms(['Montelukast','Singulair']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'zafirlukast',
            drugs =  DrugSynonym.generics_plus_synonyms(['Zafirlukast','Accolate']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'zileuton',
            drugs =  DrugSynonym.generics_plus_synonyms(['Zileuton','Zyflo']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'ipratropium',
            drugs =  DrugSynonym.generics_plus_synonyms(['Ipratropium','Atrovent']),
            exclude = ['Albuterol','Combivent','Duoneb'],
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'tiotropium',
            drugs =  DrugSynonym.generics_plus_synonyms(['Tiotropium','Spiriva']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'cromolyn-inh:generic',
            drugs =  DrugSynonym.generics_plus_synonyms(['Cromolyn',]),
            qualifiers = ['INH',' NEB', 'AER'],
            exclude = ['Intal','Gastrocrom','Nalcrom'],
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'cromolyn-inh:trade',
            drugs =  DrugSynonym.generics_plus_synonyms(['Intal', 'Gastrocrom', 'Nalcrom']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'omalizumab',
            drugs =  DrugSynonym.generics_plus_synonyms(['Omalizumab','Xolair']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'benralizumab',
            drugs =  DrugSynonym.generics_plus_synonyms(['Benralizumab','Fasenra']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'mepolizumab',
            drugs =  DrugSynonym.generics_plus_synonyms(['Mepolizumab','Nucala']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'reslizumab',
            drugs =  DrugSynonym.generics_plus_synonyms(['Reslizumab ','Cinqair']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'dupilumab',
            drugs =  DrugSynonym.generics_plus_synonyms(['Dupilumab ','Dupixent']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'tezepelumab',
            drugs =  DrugSynonym.generics_plus_synonyms(['Tezepelumab','Tezspire']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'theophylline' ,
            drugs =  DrugSynonym.generics_plus_synonyms(['Theophylline',
                                                         'Theochron',
                                                         'Elixophyllin',
                                                         'Theo-24',
                                                         'Theo-Dur',
                                                         'Uniphyl',
                                                         'Uni-Dur',
                                                         'Aminophylline',
                                                         ]),))
    
        #combinations         
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fluticasone-salmeterol:generic', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Fluticasone',]),
            require = ['Salmeterol','Fluticasone'],
            exclude = ['Advair','Airduo']
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fluticasone-salmeterol:trade',  
            drugs =  DrugSynonym.generics_plus_synonyms(['Advair','Airduo']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fluticasone-vilanterol:generic', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Fluticasone',]),
            require = ['Fluticasone','Vilanterol'],
            exclude = ['Breo','Trelegy','umeclidinium']
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fluticasone-vilanterol:trade', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Breo',]),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'albuterol-ipratropium:generic', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Albuterol',]),
            require = ['Albuterol','Ipratropium'],
            exclude = ['Combivent','Duoneb']
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'albuterol-ipratropium:trade', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Combivent','Duoneb']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'mometasone-formoterol:generic', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Mometasone',]),
            require = ['Mometasone','Formoterol'],
            exclude = ['Dulera', 'Zenhale']
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'mometasone-formoterol:trade', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Dulera','Zenhale']),
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'budesonide-formoterol:generic', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Budesonide',]),
            require = ['Budesonide','Formoterol'],
            exclude = ['Symbicort','Breyna','Wixela']
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'budesonide-formoterol:trade', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Symbicort','Breyna','Wixela']),
            ))        
        heuristic_list.append( PrescriptionHeuristic(
            name = 'budesonide-albuterol:generic', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Budesonide',]),
            require = ['Budesonide','Albuterol'],
            exclude = ['Airsupra']
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'budesonide-albuterol:trade', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Airsupra',]),
            ))        
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fluticasone-umeclidinium-vilanterol:generic', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Fluticasone',]),
            require = ['Fluticasone','Vilanterol','Umeclidinium'],
            exclude = ['Trelegy']
            ))
        heuristic_list.append( PrescriptionHeuristic(
            name = 'fluticasone-umeclidinium-vilanterol:trade', 
            drugs =  DrugSynonym.generics_plus_synonyms(['Trelegy',]),
            ))
               
        return heuristic_list
        
    def process_existing_cases(self, relevant_patients,patient_events):
        
        existing_cases = Case.objects.filter(
                patient__in = relevant_patients ,
                condition = self.conditions[0],
                ).select_related().order_by('date')

        #get distinct patients 
        patients = set()
        for case in existing_cases:
            #check to see if case has a caseactivehistory
            events = []
            patients.add(case.patient_id)
            try:
                events += patient_events[case.patient_id]
            except KeyError:
                pass
            self._update_case_from_event_list(case,
                            relevant_events = events )
        return patients

    def update_case_active_status(self):
        pre_updatable_qs = Case.objects.filter(condition=self.conditions[0]).annotate(max_date=Max('caseactivehistory__latest_event_date'))
        q1 = Q(events__date__gt=F('max_date')) # events after current case history max event date
        q2 = Q(max_date__lt=datetime.datetime.now() - relativedelta(years=2)) # or max event date is older than 2 year
        updatable_qs = pre_updatable_qs.filter(q1 | q2 ).distinct()
        counter = 0 # track number of cases updated. Not currently bothering with case history last event updates, only active status changes.
        for case in updatable_qs:
            last_date = case.max_date
            events = case.events.filter(date__gt=last_date).order_by('date')
            if case.isactive:
                disact_end = False
                updtcurhist = False
                for event in events:  #might be no events see q2 above
                    if event.date < last_date + relativedelta(years=2): # assume most new events will just support current case
                        updtcurhist = True
                        updtevnt = event
                        last_date = event.date
                    else: # got here because we're on an event that isn't within 2 years of last qualifying event
                        disact_end = last_date + relativedelta(years=2,days=-1)
                        break  
                if updtcurhist: #we just saw one or more events that did qualify as last event, so update current history with that data
                    histupdt=CaseActiveHistory.objects.get(case=case,latest_event_date=case.max_date)
                    histupdt.latest_event_date=last_date
                    histupdt.content_type = ContentType.objects.get_for_model(updtevnt)
                    histupdt.object_id = updtevnt.pk
                    histupdt.save()
                if disact_end or last_date + relativedelta(years=2) < datetime.datetime.now().date(): #have evidence to disactive case
                    last_date = disact_end if disact_end else last_date + relativedelta(years=2,days=-1)
                    case.isactive = False
                    case.inactive_date = last_date
                    case.save()
                    newhist = CaseActiveHistory(case = case,
                                                status = 'D',
                                                date = last_date,
                                                change_reason = 'E',
                                                latest_event_date = last_date,
                                                content_type = None,
                                                object_id = None,)
                    newhist.save()
                    counter += 1
            if not case.isactive:
                events = events.filter(date__gt=last_date).order_by('date')
                counter += self.create_or_update_case(events,case)
        return counter

    def create_or_update_case(self,events,case=None):
        priordxdate=None
        priorrxdate=None
        priorrxname=None
        for event in events:
            if event.name.startswith('rx'): 
                if (priorrxdate!=None and (event.date >= priorrxdate) and (event.date < priorrxdate + relativedelta(years=2))
                   and (priorrxname!=event.name or event.date>priorrxdate)):
                    #
                    # Patient has Asthma
                    #
                    t=False
                    if case==None:
                        t, case = self._create_case_from_event_list(
                            condition = self.conditions[0],
                            criteria = 'Criteria #2: Asthma >=2 prescriptions within two years',
                            recurrence_interval = None, 
                            event_obj = event,
                            relevant_event_names = events,
                            )
                    case.isactive=True
                    case.save()
                    CaseActiveHistory.create(case,
                                         ('I' if t else 'R'),
                                         event.date,
                                         'Q',
                                         event)

                    log.info('Created or reactivated asthma case: %s' % case)
                    return 1
                priorrxdate=event.date 
                priorrxname=event.name
            if event.name.startswith('dx'): 
                if priordxdate!=None and (event.date > priordxdate) and (event.date < priordxdate + relativedelta(years=2)):
                    #
                    # Patient has Asthma
                    #
                    t=False
                    if case==None:
                        t, case = self._create_case_from_event_list(
                            condition = self.conditions[0],
                            criteria = 'Criteria #1: Asthma >=2 diagnoses within two years',
                            recurrence_interval = None, 
                            event_obj = event,
                            relevant_event_names = events,
                            )
                    case.isactive=True
                    case.save()
                    CaseActiveHistory.create(case,
                                         ('I' if t else 'R'),
                                         event.date,
                                         'Q',
                                         event)
                    log.info('Created or reactivated asthma case: %s' % case)
                    return 1
                priordxdate=event.date 
        return 0

    def generate_def_ab (self):
    #
    #
           
        dx_ev_names = ['dx:asthma']
        rx_ev_names = [
            'rx:albuterol',
            'rx:levalbuterol',
            'rx:pirbuterol',
            'rx:arformoterol',
            'rx:formoterol',
            'rx:indacaterol',
            'rx:salmeterol',
            'rx:beclomethasone',
            'rx:budesonide-inh',
            'rx:pulmicort',
            'rx:ciclesonide-inh',
            'rx:alvesco',
            'rx:flunisolide-inh',
            'rx:aerobid',
            'rx:fluticasone-inh',
            'rx:flovent',
            'rx:mometasone-inh',
            'rx:asmanex',
            'rx:montelukast',
            'rx:zafirlukast',
            'rx:zileuton',
            'rx:ipratropium',
            'rx:tiotropium',
            'rx:cromolyn-inh:generic',
            'rx:cromolyn-inh:trade',
            'rx:omalizumab',
            'rx:benralizumab',
            'rx:mepolizumab',
            'rx:reslizumab',
            'rx:dupilumab',
            'rx:tezepelumab',
            'rx:theophylline',
        ]
        
        rx_comb_ev_names = [
             'rx:fluticasone-salmeterol:generic',
             'rx:fluticasone-vilanterol:generic',
             'rx:albuterol-ipratropium:generic', 
             'rx:mometasone-formoterol:generic',
             'rx:budesonide-formoterol:generic',
             'rx:fluticasone-salmeterol:trade',
             'rx:fluticasone-vilanterol:trade',
             'rx:albuterol-ipratropium:trade', 
             'rx:mometasone-formoterol:trade',
             'rx:budesonide-formoterol:trade',
             'rx:budesonide-albuterol:generic',
             'rx:budesonide-albuterol:trade',
             'rx:fluticasone-umeclidinium-vilanterol:generic',
             'rx:fluticasone-umeclidinium-vilanterol:trade'
            ]
        log.info('Generating cases for Asthma')

        counter = 0
        all_event_names =  rx_ev_names + rx_comb_ev_names + dx_ev_names
               

        all_qs = BaseEventHeuristic.get_events_by_name(name=all_event_names).exclude(case__condition=self.conditions[0]).order_by('patient','date').select_related()
        log.info('Queryset all_qs created')
        #distinct patients
        patcount=0
        last_id=0 
        patients = set()
        patient_events = {}
        for event in all_qs.iterator():
            if event.patient_id != last_id and patcount < 1000:
                patcount+=1
                last_id=event.patient_id
            elif event.patient_id != last_id and patcount == 1000:
                #for every 1000 patients we run the case processing
                
                log.info('At patcount = 1000.  Case ounter is currently ' + str(counter))

                patients_with_existing_cases = self.process_existing_cases(patients,patient_events)
        
                for patient in patients - set(patients_with_existing_cases): 
                    counter += self.create_or_update_case(patient_events[patient])

                patients = set()
                patient_events = {}
                patcount=1
                last_id=event.patient_id
            patients.add(event.patient_id)
            try:
                patient_events[event.patient_id].append(event)
            except:
                patient_events[event.patient_id] = [event]
        #This handles the last set of fewer than 1000
        patients_with_existing_cases = self.process_existing_cases(patients,patient_events)
        
        for patient in patients - set(patients_with_existing_cases): 
            counter += self.create_or_update_case(patient_events[patient])
       
        return counter # Count of new cases

    def generate(self):
        log.info('Generating cases of %s' % self.short_name)
        counter = 0
        counter += self.generate_def_ab()
        log.debug('Generated %s new cases of asthma' % counter)
        updates = self.update_case_active_status()
        log.debug('Updated %s existing cases of asthma' % updates)
        
        return counter # Count of new cases
    
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------


def event_heuristics():
    return AsthmaNew().event_heuristics

def disease_definitions():
    return [AsthmaNew()]
